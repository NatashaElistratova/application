(function(ng) {

  ng.module('app')
    .service('ComService', ComService);

  function ComService($http) {
    this.$http = $http;
    this.items = [];
    this.chapters=[];
  };

  ComService.prototype.chapterList = function() {
    var _this = this;
          return this.$http.get('http://localhost:3000/chapters')
              .then(function(response){
                _this.chapters = response.data;
                console.log();
                return _this.chapters;
              })
              .catch(function(e) {
                alert(JSON.stringify(e));
              })
      };

  ComService.prototype.list = function() {
    var _this = this;
          return this.$http.get('http://localhost:3000/comments')
              .then(function(response){
                _this.items = response.data;
                return _this.items;
              })
              .catch(function(e) {
                alert(JSON.stringify(e));
              })
      };

  ComService.prototype.remove = function (comment) {
    var id = comment.id;
    var _this = this;
          return this.$http.delete('http://localhost:3000/comments/' + id)
              .then(function(response){
                _this.items = response.data;
                return _this.items;
              })
              .catch(function(e) {
                alert(JSON.stringify(e));
              })

  }

  ComService.prototype.add = function () {
    var addButton = document.getElementById('addButton');
    var value  = addButton.value;
    var comment = {
      "id":"",
      "text":value,
      "rate":"0",
      "author": "Username"
    };
    addButton.value = '';
    var _this = this;
          return this.$http.post('http://localhost:3000/comments/', comment)
              .then(function(response){
                _this.items = response.data;
                return _this.items;
              })
              .catch(function(e) {
                alert(JSON.stringify(e));
              })

  }

  ComService.prototype.plus = function (comment) {
    var id = comment.id;
    var _this = this;
    comment.rate++;
          return this.$http.put('http://localhost:3000/comments/' + id, comment)
              .then(function(response){
                _this.item = response.data;
                return _this.item;
              })
              .catch(function(e) {
                alert(JSON.stringify(e));
              })

  }

  ComService.prototype.minus = function (comment) {
    var id = comment.id;
    var _this = this;
    comment.rate--;
          return this.$http.put('http://localhost:3000/comments/' + id, comment)
              .then(function(response){
                _this.item = response.data;
                return _this.item;
              })
              .catch(function(e) {
                alert(JSON.stringify(e));
              })

  }


  ComService.prototype.edit = function (comment) {
    var id = comment.id;
    var _this = this;

    return this.$http.put('http://localhost:3000/comments/' + id, comment)
        .then(function(response){
          _this.item = response.data;
          return _this.item;
        })
        .catch(function(e) {
          alert(JSON.stringify(e));
        })

  };


})(angular);
