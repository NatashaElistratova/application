(function(ng){
  ng.module('app')
    .directive('menu', MenuDirective)
    .filter('orderObjectBy', function() {
            return function(items, field, reverse) {
                var filtered = [];
                angular.forEach(items, function(item) {
                    filtered.push(item);
                });
                filtered.sort(function(a, b) {
                    return (a[field] > b[field] ? 1 : -1);
                });
                if (reverse) filtered.reverse();
                return filtered;
            };
        });


  function MenuController(ComService){
    this.ComService = ComService;
  };

  MenuController.prototype.$onInit = function() {
    var that = this;
     this.ComService.chapterList().then(function(result) {
       that.chapters= result;
       return that.chapters;
     });
   }


  function MenuDirective(){
    return {
      restrict: 'E',
      controller: MenuController,
      controllerAs: '$ctrlMenu',
      templateUrl: 'js/menu/template.html',
      bindToController: true,


    }
  }
})(angular);
