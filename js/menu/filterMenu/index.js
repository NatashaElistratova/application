(function(ng){
  ng.module('app')
    .directive('menuFilter', MenuFilterDirective);

  function MenuFilterController(){};

  function MenuFilterDirective(){
    return {
      restrict: 'E',
      controller: MenuFilterController,
      controllerAs: '$ctrlMenuFilter',
      templateUrl: 'js/menu/filterMenu/template.html',
      bindToController: true,
      require: {
        MenuController: '^^menu'
      }

    }
  }
})(angular);
