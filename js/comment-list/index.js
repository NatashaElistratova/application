(function(ng) {

  ng.module('app')
    .directive('commentList', commentListDirective);

  function ComListController(ComService) {
    this.ComService = ComService;
  }


  ComListController.prototype.$onInit = function() {
    var that = this;
     this.ComService.list().then(function(result) {
       that.items = result;
       return that.items;
     });
   }

  ComListController.prototype.remove = function (comment) {
    var that = this;
    this.ComService.remove(comment).then(function(result) {
      that.items = result;
      that.$onInit();
    });

  };

  ComListController.prototype.add = function (comment) {
    var that = this;
    this.ComService.add(comment).then(function(result) {
      that.items = result;
      that.$onInit();
    });
  };

  function commentListDirective() {
    return {
      restrict: 'E',
      controller: ComListController,
      controllerAs: '$ctrl',
      templateUrl: 'js/comment-list/template.html',
      bindToController: true,

    }
  }
})(angular)
