(function(ng) {

  ng.module('app')
    .directive('comment', CommentDirective);

  function ComController(ComService) {
    this.ComService = ComService;
  }

  ComController.prototype.remove = function () {
    this.ComListController.remove(this.item);
  };

  ComController.prototype.$onInit = function () {
    this.ComListController.$onInit();
  };

  ComController.prototype.plus = function () {
    var that = this;
    this.ComService.plus(this.item).then(function(result) {
      that.item = result;
      that.$onInit();
    });
  }

  ComController.prototype.minus = function () {
    var that = this;
    this.ComService.minus(this.item).then(function(result) {
      that.item = result;
      that.$onInit();
    });
  }

  ComController.prototype.edit = function () {
    var that = this;
    this.ComService.edit(this.item).then(function(result) {
      that.item = result;
      that.$onInit();
    });

  };


  function CommentDirective() {
    return {
      restrict: 'E',
      controller: ComController,
      controllerAs: '$ctrl',
      templateUrl: 'js/comment-list/comment/template.html',
      bindToController: true,
      scope: {
        item: '='
      },
      require: {
        ComListController: '^^commentList'
      }
    }
  }
})(angular)
