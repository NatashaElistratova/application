(function(ng) {

  ng.module('app')
    .directive('commentEnter', ComEnterDirective);

  function ComEnterController() {
  }

  ComEnterController.prototype.add = function () {
    this.ComListController.add();
  };

  function ComEnterDirective() {
    return {
      restrict: 'E',
      controller: ComEnterController,
      controllerAs: '$ctrlEnter',
      templateUrl: 'js/comment-list/comment-enter/template.html',
      bindToController: true,

      require: {
        ComListController: '^^commentList'
      }
    }
  }
})(angular)
