(function(ng){
  ng.module('app')
    .directive('sidebar', SidebarDirective)

  function SidebarController(){

  };

  SidebarController.prototype.showSidebar = function(){
    var sidebar = document.getElementById('sidebar');
    angular.element(sidebar).css('display','block');
  }


  function SidebarDirective(){
    return {
      restrict: 'E',
      controller: SidebarController,
      controllerAs: '$ctrlSidebar',
      templateUrl: 'js/sidebar/template.html',
      bindToController: true,


    }
  }
})(angular);
