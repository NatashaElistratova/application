(function(ng){
  ng.module('app')
    .directive('search', SearchDirective);

  function SearchController(){};

  SearchController.prototype.showInput = function(){
    this.right = 0;
    this.rightButton = 320;
  }

  function SearchDirective(){
    return {
      restrict: 'E',
      controller: SearchController,
      controllerAs: '$ctrlSearch',
      templateUrl: 'js/search/template.html',
      bindToController: true,

    }
  }
})(angular);
